# Q3. Write out all prime numbers smaller than 1000 to a text file, one per line. Read back the text file and count all twin-primes in it. Twin primes are pairs of primes that differ by 2, for example: (5,7) and (11,13) and (41,43).
primes = []
def findPrimes():
  for a in range(2,1001):
    quotient = 0
    for b in range(2,a):
      if a%b == 0:
        quotient = quotient + 1
    if quotient == 0:
      primes.append(a)
  printToFile(primes)


def printToFile(primes):
  myFile = open("primesfile.txt", "w")
  for index,i in enumerate(primes):
    j = str(i)
    myFile.write(j)
    myFile.write("\n")
  myFile.close()


def twinPrimes(primes):
  print("Twin primes up to 1000:")
  for i in range(len(primes)):
    for j in range(i+1,len(primes)):
      if abs(primes[i]-primes[j])==2:
        a = primes[i]
        b = primes[i+1]
        print(a,b)


def readPrimes():
  prime = []
  with open("primesfile.txt") as primeFile:
    for i in primeFile:
      j = int(i)
      prime.append(j)
  primeFile.close()
  twinPrimes(prime)
findPrimes()
readPrimes()

# def findprimes():
#   for num in range(2, 1001):
#     for i in range
# findPrimes()
# twinPrimes()